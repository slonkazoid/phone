#![feature(let_chains)]

use std::{
    env,
    fs::OpenOptions,
    process::{Command, Stdio},
    time::SystemTime,
};

use axum::{
    extract::State,
    http::{HeaderMap, StatusCode},
    response::{IntoResponse, Response},
    routing::get,
    Router,
};
use chrono::{DateTime, Utc};
use thiserror::Error;
use tokio::net::TcpListener;
use tower::limit::ConcurrencyLimitLayer;
use tracing::error;

macro_rules! silly {
    ($code:ident) => {
        (StatusCode::$code, StatusCode::$code.to_string())
    };
}

struct Config {
    pub host: String,
    pub port: u16,
}

#[derive(Debug, Error)]
enum AppError {
    #[error("Command failed to execute (exit code {0})")]
    CommandFailed(i32),
    #[error(transparent)]
    IoError(#[from] std::io::Error),
}

impl IntoResponse for AppError {
    fn into_response(self) -> Response {
        error!("{}", self);
        silly!(INTERNAL_SERVER_ERROR).into_response()
    }
}

async fn update_website(
    State(secret): State<String>,
    headers: HeaderMap,
) -> Result<StatusCode, AppError> {
    if let Some(secret_header) = headers.get("x-gitlab-token")
        && *secret_header == secret
    {
        let now = SystemTime::now();
        let date: DateTime<Utc> = now.into();

        let file_name = format!("git_output_{}", date);

        let stdout_file = OpenOptions::new()
            .create(true)
            .write(true)
            .open(&file_name)
            .unwrap();
        let stderr_file = OpenOptions::new().write(true).open(&file_name).unwrap();

        #[allow(deprecated)]
        let result = Command::new("git")
            .current_dir(env::home_dir().unwrap().join("slonksite"))
            .arg("pull")
            .stdin(Stdio::null())
            .stdout(stdout_file)
            .stderr(stderr_file)
            .spawn()?
            .wait()
            .unwrap();

        if result.success() {
            Ok(StatusCode::OK)
        } else {
            Err(AppError::CommandFailed(result.code().unwrap_or(1)))
        }
    } else {
        Ok(StatusCode::UNAUTHORIZED)
    }
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    let config = Config {
        host: env::var("HOST").unwrap_or("0.0.0.0".to_string()),
        port: env::var("PORT")
            .and_then(|port| Ok(port.parse::<u16>().unwrap()))
            .unwrap_or(5000),
    };

    let app = Router::new()
        .route("/update_website", get(update_website))
        .layer(ConcurrencyLimitLayer::new(8))
        .with_state(env::var("SECRET").unwrap());

    let address = (config.host, config.port);
    let listener = TcpListener::bind(address).await.unwrap();
    let local_addr = listener.local_addr().unwrap();
    tracing::info!(
        "listening on http://{}:{}",
        local_addr.ip(),
        local_addr.port()
    );

    axum::serve(listener, app).await.unwrap();
}
